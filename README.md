<div align="center">
<img align="center" src="src/main/resources/ou-all.png" alt="Fn">
</div>

<div align="center">

<div style="display: flex; justify-content: center;">  
    <img style="margin: 5px 3px" src="https://gitee.com/FnTop/openutil/badge/star.svg?theme=light" alt="Fn">
    <img style="margin: 5px 3px" src="https://gitee.com/FnTop/openutil/badge/fork.svg?theme=dark" alt="Fn">
    <img style="margin: 5px 3px" src="https://img.shields.io/badge/Version-2.0.2-green" alt="Fn">
    <img style="margin: 5px 3px" src="https://img.shields.io/badge/Apache-License2.0-green" alt="Apache2.0">

</div>

<div style="display: flex; justify-content: center;">  
    <img style="margin: 5px 3px" src="https://img.shields.io/badge/Maven-3.5.2-blue" alt="Maven">
    <img style="margin: 5px 3px" src="https://img.shields.io/badge/Java-1.8-blue">
    <img style="margin: 5px 3px" src="https://img.shields.io/badge/SpringBoot-2.6.2-blue">
    <img style="margin: 5px 3px" src="https://img.shields.io/badge/Author-4n-blue.svg" alt="Fn">
</div>

</div>

# 介绍

对外接口快速封装的 `spring boot starter`，这个`starter`是为了方便将对外接口进行快速封装并提供给外部调用。
以注解方式进行快速封装，外部以`方法api`的方式调用，摒弃传统对接方封装httpclient工具的繁琐流程，加快对接速度。框架支持丰富的自定义扩展。

* [官方文档](http://openutil.fntop.cn/)
* [Api文档](https://apidoc.gitee.com/FnTop/openutil)
* [使用案例](https://gitee.com/FnTop/fn-sms)

开源不易，大家帮忙点个`start`支持下

# 特点

* 快：对外接口封装快，外部对接速度快
* 新：对外接口文档新写法(面向方法api的接口文档)
* 丰富的自定义扩展
* 多业务分类处理
* 自定义序列化策略
* 支持多线程
* 支持拦截器
* 支持代理

## 版本说明
| version | spring boot version | java version |
|:-------:|:-------------------:|:------------:|
|  2.0.2  |        2.6.2        |     1.8      |

## Maven
```xml
<dependency>
    <groupId>cn.fntop</groupId>
    <artifactId>open-api-spring-boot-starter</artifactId>
    <version>2.0.2</version>
</dependency>
```
## Gradle
``` 
//方式1
implementation 'cn.fntop:open-api-spring-boot-starter:2.0.2'
//方式2
implementation group: 'cn.fntop', name: 'open-api-spring-boot-starter', version: '2.0.2'
```
# 配置提示
![img.png](img.png)

# 配置

```yaml

fn:
#接口网关，域名+your-context，如果没有context就不用加context，尾部一定要加上`/`
  open-gateway: http://xxx.com/your-context/ 
  #代理
  proxy-domain: xxx
  proxy-port: xxx
```

## 配置说明
| 配置项                          | 默认值                          | 说明                                        |
|:-----------------------------|:-----------------------------|:------------------------------------------|
| fn.open-gateway | http://xxx.com/your-context/ | 接口调用网关，默认http://127.0.0.1:8080/。末尾一定要加"/" |
| fn.proxy-domain | 127.0.0.1                    | 代理主机                                |
| fn.proxy-port | 38672                        | 代理端口                                      |


# 默认方法Api `OpenApiService`
添加依赖项后注入`OpenApiService`即可

```java

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Demo {
    private final OpenApiService openApiService;
}

//或者
@Service
public class Demo {
    @Autowired
    private OpenApiService openApiService;
    public Object method1(){
        //参数为接口地址
        openApiService.get("hello/custom");
    }

}

```
# 自定义扩展

1. 自定义配置

```java
@Configuration
@RequiredArgsConstructor
public class CustomConfig {
    private final OpenApiProperties properties;
    @Bean
    public CustomOpenApiService customOpenApiService() {
        return (CustomOpenApiService) CustomOpenApiService.getInstance(properties, CustomOpenApi.class, CustomOpenApiService.class);
    }
}
```

2. 自定义开放接口  `extends OpenApi`

```java
public interface CustomOpenApi extends OpenApi {
    @GET("hello/custom")
    Single<Object> callCustom();
}
```

3. 自定义Service `extends OpenApiService` , 继承后可沿用OpenApiService默认方法

```java
public class CustomOpenApiService extends OpenApiService {
    private CustomOpenApi api;
    public CustomOpenApiService() {
        this.api = (CustomOpenApi) OpenApiService.api;
    }
    public Object callCustom() {
        return execute(api.callCustom());
    }
}
```

4. 写好工具类后发布到私服。第三方调用直接通过依赖你发布到私服的jar包并通过Spring注入你自定义的`customOpenApiService`即可调用。

```java
@Service
@RequiredArgsConstructor
public class Demo {
    private final CustomOpenApiService customOpenApiService;
}

```
# 案例解析
比如短信平台使用openutil后，是怎么提供短信发送接口能力给对接方（调用短信接口方）的

1. 短信方依赖openutil

2. 短信方实现自定义扩展，使用注解完成方法api封装

3. 短信方发布依赖

4. 对接方引用短信方封装的依赖

5. 对接方配置短信方网关地址

6. 对接方使用短信方的方法api


> 对接方配置网关地址

``` yaml
fn:
  open-gateway: http://xxx.com/your-context/  #
```
> 对接方引用依赖

```xml
<dependency>
    <groupId>cn.fntop</groupId>
    <artifactId>sms-core-spring-boot-starter</artifactId>
    <version>2.0.0</version>
</dependency>

```
> 对接方使用方法api

``` java
//1、自动注入该对象
private final Sms sms;
//2、构建短信对象
Dict map = Dict.create();
map.put("code", "1654");
SmsRequest request = SmsRequest.builder().sid("签名id")
.templateId("模板id").phone("手机号").appId("注册的会员id").data(map).build();
//3、调用
sms.send(smsRequest);

```
# 群聊
`QQ群：697135336`
`微信：gensui_`





