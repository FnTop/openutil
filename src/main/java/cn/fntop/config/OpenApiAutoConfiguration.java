package cn.fntop.config;

import cn.fntop.service.OpenApi;
import cn.fntop.service.OpenApiService;
import cn.fntop.service.OpenApiServiceAbstract;
import cn.fntop.service.OpenApiServiceSubject;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动注入
 *
 * @author fn
 */
@Configuration
@EnableConfigurationProperties(OpenApiProperties.class)
@RequiredArgsConstructor
public class OpenApiAutoConfiguration {
    private final OpenApiProperties properties;

    /**
     * 如果要自定义实例，请调用{@link OpenApiService}的静态方法custom()
     * custom()方法可设置拦截器和翻墙代理
     * @return
     */
    @Bean
    public OpenApiService openApiService() {
        return (OpenApiService) OpenApiService.getInstance(properties, OpenApi.class, OpenApiService.class);
    }

}
