package cn.fntop.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author fn
 * @description
 * @date 2023/3/5 22:49
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "fn")
public class OpenApiProperties {
    /**
     * 接口调用网关
     */
    private String openGateway = "http://127.0.0.1:8080/";
    /**
     * 代理主机
     */
    private String proxyDomain;
    /**
     * 代理端口
     */
    private int proxyPort;

}
